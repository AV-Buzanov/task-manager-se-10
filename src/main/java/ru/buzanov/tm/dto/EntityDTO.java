package ru.buzanov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<Project> projects;
    private List<Task> tasks;
    private List<User> users;

    public void load(@Nullable final ServiceLocator serviceLocator) {
        if (serviceLocator==null) return;
        this.projects = new ArrayList<>(serviceLocator.getProjectService().findAll());
        this.tasks = new ArrayList<>(serviceLocator.getTaskService().findAll());
        this.users = new ArrayList<>(serviceLocator.getUserService().findAll());
    }
}
