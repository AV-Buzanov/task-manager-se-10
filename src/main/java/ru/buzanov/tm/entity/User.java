package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.buzanov.tm.enumerated.RoleType;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {
    private String name;
    private String login;
    private String passwordHash;
    private RoleType roleType;

    public User(String name, String login, String pass, RoleType roleType) {
        this.name = name;
        this.login = login;
        this.passwordHash = pass;
        this.roleType = roleType;
    }
}
