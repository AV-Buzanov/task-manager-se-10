package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.buzanov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractWBS extends AbstractEntity {
    private String name;
    private String userId;
    private Status status = Status.PLANNED;
    private Date createDate = new Date(System.currentTimeMillis());
    private Date startDate;
    private Date finishDate;
    private String description;
}