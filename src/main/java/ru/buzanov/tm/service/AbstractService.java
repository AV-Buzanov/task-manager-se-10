package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IRepository;
import ru.buzanov.tm.api.service.IService;
import ru.buzanov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    private IRepository<T> abstractRepository;

    AbstractService(final IRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    public T load(@Nullable final T entity) {
        if (entity == null)
            return null;
        return abstractRepository.load(entity);
    }

    @NotNull
    public Collection<T> findAll() {
        return abstractRepository.findAll();
    }

    public void load(@Nullable final List<T> list) {
        if (list == null || list.isEmpty())
            return;
        abstractRepository.load(list);
    }

    @Nullable
    public T findOne(@Nullable final String id) {
        if (id == null || id.isEmpty())
            return null;
        return abstractRepository.findOne(id);
    }

    public void merge(@Nullable final String id, @Nullable final T entity) {
        if (id == null || id.isEmpty())
            return;
        if (entity == null)
            return;
        abstractRepository.merge(id, entity);
    }

    @Nullable
    public T remove(@Nullable final String id) {
        if (id == null || id.isEmpty())
            return null;
        return abstractRepository.remove(id);
    }

    public void removeAll() {
        abstractRepository.removeAll();
    }
}
