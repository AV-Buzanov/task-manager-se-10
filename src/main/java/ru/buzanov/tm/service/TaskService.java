package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.entity.Task;

import java.util.Collection;

@NoArgsConstructor
public class TaskService extends AbstractWBSService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    public Collection<Task> findByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty())
            return null;
        return this.taskRepository.findByProjectId(projectId);
    }

    public void removeByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty())
            return;
        taskRepository.removeByProjectId(projectId);
    }

    @Nullable
    public Collection<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty())
            return null;
        if (projectId == null || projectId.isEmpty())
            return null;
        return taskRepository.findByProjectId(userId, projectId);
    }

    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty())
            return;
        if (projectId == null || projectId.isEmpty())
            return;
        taskRepository.removeByProjectId(userId, projectId);
    }
}