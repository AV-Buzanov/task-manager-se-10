package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IWBSRepository;
import ru.buzanov.tm.api.service.IWBSService;
import ru.buzanov.tm.entity.AbstractWBS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

@NoArgsConstructor
public abstract class AbstractWBSService<T extends AbstractWBS> extends AbstractService<T> implements IWBSService<T> {
    private IWBSRepository<T> abstractSubjectRepository;

    AbstractWBSService(final IWBSRepository<T> abstractRepository) {
        super(abstractRepository);
        this.abstractSubjectRepository = abstractRepository;
    }

    @NotNull
    public Collection<T> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        return abstractSubjectRepository.findAll(userId);
    }

    @NotNull
    public Collection<T> findByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty())
            return new ArrayList<>();
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        return abstractSubjectRepository.findByName(userId, name);
    }

    @NotNull
    public Collection<T> findByDescription(@Nullable final String userId, @Nullable final String desc) {
        if (desc == null || desc.isEmpty())
            return new ArrayList<>();
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        return abstractSubjectRepository.findByDescription(userId, desc);
    }

    @Nullable
    public T findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.findOne(userId, id);
    }

    public boolean isNameExist(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty())
            return false;
        if (name == null || name.isEmpty())
            return false;
        return abstractSubjectRepository.isNameExist(userId, name);
    }

    public void setName(@Nullable final String name, @Nullable final T entity) throws Exception {
        if (entity==null)
            return;
        if (name == null)
            return;
        if (isNameExist(entity.getUserId(), name))
            throw new Exception("This name already exist.");
        if (name.isEmpty()&&entity.getName()==null)
            throw new Exception("Name can't be empty.");
        if (!name.isEmpty())
            entity.setName(name);
    }

    @NotNull
    public String getList() {
        return abstractSubjectRepository.getList();
    }

    @Nullable
    public String getList(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getList(userId);
    }

    @Nullable
    public String getIdByCount(final int count) {
        return abstractSubjectRepository.getIdByCount(count);
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getIdByCount(userId, count);
    }

    public void merge(@Nullable final String userId, @Nullable final String id, @Nullable final T entity) {
        if (userId == null || userId.isEmpty())
            return;
        if (id == null || id.isEmpty())
            return;
        if (entity == null)
            return;
        abstractSubjectRepository.merge(userId, id, entity);
    }

    @Nullable
    public T remove(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.remove(userId, id);
    }

    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        abstractSubjectRepository.removeAll(userId);
    }

    @NotNull
    public Comparator<T> getNameComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (direction)
                    return o1.getName().compareToIgnoreCase(o2.getName());
                return o1.getName().compareToIgnoreCase(o2.getName()) * (-1);
            }
        };
    }

    @NotNull
    public Comparator<T> getStartDateComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (o1.getStartDate() == null && o2.getStartDate() == null)
                    return 0;
                if (o1.getStartDate() == null && o2.getStartDate() != null)
                    return 1;
                if (o1.getStartDate() != null && o2.getStartDate() == null)
                    return -1;
                if (direction) {
                    assert o1.getStartDate() != null;
                    return o1.getStartDate().compareTo(o2.getStartDate());
                }
                assert o1.getStartDate() != null;
                return o1.getStartDate().compareTo(o2.getStartDate()) * (-1);
            }
        };
    }

    @NotNull
    public Comparator<T> getEndDateComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (o1.getFinishDate() == null && o2.getFinishDate() == null)
                    return 0;
                if (o1.getFinishDate() == null && o2.getFinishDate() != null)
                    return 1;
                if (o1.getFinishDate() != null && o2.getFinishDate() == null)
                    return -1;
                if (direction) {
                    assert o1.getFinishDate() != null;
                    return o1.getFinishDate().compareTo(o2.getFinishDate());
                }
                assert o1.getFinishDate() != null;
                return o1.getFinishDate().compareTo(o2.getFinishDate()) * (-1);
            }
        };
    }

    @NotNull
    public Comparator<T> getStatusComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (direction)
                    return o1.getStatus().compareTo(o2.getStatus());
                return o1.getStatus().compareTo(o2.getStatus()) * (-1);
            }
        };
    }
}
