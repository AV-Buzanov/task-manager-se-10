package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.repository.UserRepository;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;
import ru.buzanov.tm.util.TerminalService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IUserService userService = new UserService(userRepository);
    private final ITerminalService terminalService = new TerminalService();
    private final Map<String, AbstractCommand> commands = new TreeMap<>();
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.buzanov.tm").getSubTypesOf(AbstractCommand.class);

    @SneakyThrows
    private void init() {
        for (Class aClass : classes) {
            registryCommand((AbstractCommand) aClass.newInstance());
        }
        registryUser(new User("UserName", "user", "123456", RoleType.USER));
        registryUser(new User("AdminName", "admin", "123456", RoleType.ADMIN));
        userService.setCurrentUser(userService.findByLogin("user"));

    }

    private void registryCommand(@NotNull final AbstractCommand command) {
        if (command.command() == null || command.command().isEmpty()) {
            return;
        }
        if (command.description() == null || command.description().isEmpty()) {
            return;
        }

        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }

    private void registryUser(@NotNull final User user) {
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            return;
        }
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty()) {
            return;
        }
        if (user.getRoleType() == null) {
            return;
        }
        userService.load(user);
    }

    public void start() {
        init();

        String command = "";

        terminalService.printLineG("***WELCOME TO TASK MANAGER***");
        terminalService.printLine("Type help to see command list.");

        while (!commands.get("exit").command().equals(command)) {
            try {
                command = terminalService.readLine();
                if (!command.isEmpty()) {

                    if (userService.getCurrentUser() == null && commands.get(command).isSecure())
                        terminalService.printLineR("Authorise, please");
                    else {
                        if (userService.getCurrentUser() != null && !commands.get(command).isRoleAllow(userService.getCurrentUser().getRoleType()))
                            terminalService.printLineR("This command is not allow for you");
                        else
                            commands.get(command).execute();
                    }
                }

            } catch (Exception e) {
                terminalService.printLineR(e.getMessage());
            }
        }
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}