package ru.buzanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {
    PLANNED("Запланированно"),
    IN_PROGRESS("В процессе"),
    DONE("Завершено");

    private final String name;

    Status(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
