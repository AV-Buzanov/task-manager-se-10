package ru.buzanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {
    ADMIN("Администратор"),
    USER("Пользователь");

    private final String name;

    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
