package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository extends IWBSRepository<Task> {

    @NotNull Collection<Task> findByProjectId(@NotNull final String projectId);

    void removeByProjectId(@NotNull final String projectId);

    @NotNull Collection<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId);

    void removeByProjectId(@NotNull final String userId, @NotNull final String projectId);
}
