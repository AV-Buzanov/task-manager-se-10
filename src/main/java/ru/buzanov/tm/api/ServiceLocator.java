package ru.buzanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.util.TerminalService;

import java.util.Collection;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

    @NotNull Collection<AbstractCommand> getCommands();

    @NotNull ITerminalService getTerminalService();
}
