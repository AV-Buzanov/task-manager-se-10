package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;

import java.util.Objects;

public class UserViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View user information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[VIEW USER]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        terminalService.printLineG("[LOGIN]");
        terminalService.printLine(userService.getCurrentUser().getLogin());
        terminalService.printLineG("[NAME]");
        terminalService.printLine(userService.getCurrentUser().getName());
        terminalService.printLineG("[ROLE]");
        terminalService.printLine(userService.getCurrentUser().getRoleType().displayName());
        terminalService.printLineG("[PROJECTS COUNT]");
        terminalService.printLine(String.valueOf(Objects.requireNonNull(projectService.findAll(userId)).size()));
        terminalService.printLineG("[TASKS COUNT]");
        terminalService.printLine(String.valueOf(Objects.requireNonNull(taskService.findAll(userId)).size()));
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
