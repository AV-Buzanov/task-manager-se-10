package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Objects;

public class UserRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected user";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE USER TO REMOVE]");
        @NotNull final User[] users = new User[Objects.requireNonNull(userService.findByRole(RoleType.USER)).size()];
        Objects.requireNonNull(userService.findByRole(RoleType.USER)).toArray(users);
        for (int i = 0; i < users.length; i++) {
            terminalService.print(String.valueOf(i + 1));
            terminalService.printLine(": ", users[i].getLogin(), " " + users[i].getName());
        }
        int indexBuf = Integer.parseInt(terminalService.readLine()) - 1;
        terminalService.printLineG("[ARE YOU SURE(Y/N)?]");
        if ("Y".equals(terminalService.readLine())) {
            userService.remove(users[indexBuf].getId());
            projectService.removeAll(users[indexBuf].getId());
            taskService.removeAll(users[indexBuf].getId());
            terminalService.printLineG("[USER REMOVED]");
        }
        terminalService.printLineG("[CANCELED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.USER.equals(role))
            return false;
        return super.isRoleAllow(role);
    }
}
