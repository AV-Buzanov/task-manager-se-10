package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

public class UserListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "View user list (for admin only)";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[USER LIST]");
        for (@NotNull final User user : userService.findAll()) {
            terminalService.printG("[LOGIN] ");
            terminalService.print(user.getLogin());
            terminalService.printG(" [NAME] ");
            terminalService.print(user.getName());
            terminalService.printG(" [ROLE] ");
            terminalService.printLine(user.getRoleType().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.USER.equals(role))
            return false;
        return super.isRoleAllow(role);
    }
}
