package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;

import java.util.Objects;

public class UserAuthCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "auth";
    }

    @NotNull
    @Override
    public String description() {
        return "User authentication";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[AUTHORISATION]");
        terminalService.printLineG("[ENTER LOGIN]");
        @NotNull String stringBuf = terminalService.readLine();
        if (stringBuf.isEmpty()) {
            terminalService.printLine("Login can't be empty");
            return;
        }
        if (!userService.isLoginExist(stringBuf)) {
            terminalService.printLine("User doesn't exist, register please");
            return;
        }
        @NotNull final User user = userService.findByLogin(stringBuf);
        terminalService.printLineG("[ENTER PASS]");
        stringBuf = terminalService.readLine();

        if (!userService.isPassCorrect(Objects.requireNonNull(user).getLogin(), stringBuf)) {
            terminalService.printLine("Invalid pass");
            return;
        }
        userService.setCurrentUser(user);
        terminalService.printLineG("[HELLO, " + user.getName() + ", NICE TO SEE YOU!]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
