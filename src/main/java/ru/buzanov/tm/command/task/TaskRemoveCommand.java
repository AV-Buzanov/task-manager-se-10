package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;

import java.util.Objects;

public class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE TASK TO REMOVE]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        terminalService.printLine(taskService.getList(userId));
        @Nullable String idBuf = taskService.getIdByCount(userId, Integer.parseInt(terminalService.readLine()));
        taskService.remove(userId, Objects.requireNonNull(idBuf));
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
