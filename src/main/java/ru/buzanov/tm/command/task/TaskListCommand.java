package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Task;

import java.util.Objects;

public class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[TASK LIST]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        for (@NotNull final Task task : Objects.requireNonNull(taskService.findAll(userId))) {
            terminalService.printWBS(task);
            terminalService.printG(" [PROJECT] ");
            terminalService.printLine(projectService.findOne(userId, task.getProjectId()).getName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
