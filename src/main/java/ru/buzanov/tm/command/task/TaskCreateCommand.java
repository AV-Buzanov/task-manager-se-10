package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Task;

import java.util.Objects;

public class TaskCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Task task = new Task();
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        terminalService.printLineG( "[TASK CREATE]");
        terminalService.printG( "[ENTER NAME]");
        @NotNull String stringBuf = terminalService.readLine();
        taskService.setName(stringBuf, task);
        terminalService.printLine("[FILL DATA(Y/N)?]");
        if (terminalService.readLine().equals("Y"))
            terminalService.readWBS(task);
        if (!projectService.findAll(userId).isEmpty()) {
            terminalService.printLineG( "[CHOOSE PROJECT]");
            terminalService.printLine(projectService.getList(userId));
            stringBuf = terminalService.readLine();
            if (!stringBuf.isEmpty()) {
                task.setProjectId(projectService.getIdByCount(userId, Integer.parseInt(stringBuf)));
            }
        }
        task.setUserId(userId);
        taskService.load(task);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
