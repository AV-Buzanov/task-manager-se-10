package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskFindNameCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-find";
    }

    @NotNull
    @Override
    public String description() {
        return "Find tasks by name, description or project name.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        List<Task> list = new ArrayList<>();
        terminalService.printLineG("[FIND BY]");
        terminalService.printLine("1 : Name");
        terminalService.printLine("2 : Description");
        terminalService.printLine("3 : Project name");
        @NotNull final String s = terminalService.readLine();
        switch (s) {
            case ("1"):
                terminalService.printLineG("[ENTER NAME]");
                list = (List<Task>) taskService.findByName(userId, terminalService.readLine());
                break;
            case ("2"):
                terminalService.printLineG("[ENTER DESCRIPTION]");
                list = (List<Task>) taskService.findByDescription(userId, terminalService.readLine());
                break;
            case ("3"):
                terminalService.printLineG("[ENTER PROJECT NAME]");
                @NotNull final String projectname = terminalService.readLine();
                for (Project project : Objects.requireNonNull(projectService.findByName(userId, projectname)))
                    list.addAll(Objects.requireNonNull(taskService.findByProjectId(userId, project.getId())));
                break;
            default:
                break;
        }
        if (list.isEmpty())
            terminalService.printLine(FormatConst.EMPTY_FIELD);
        else {
            for (@NotNull final Task task : list) {
                terminalService.printWBS(task);
                terminalService.printG("[PROJECT] ");
                terminalService.printLine(projectService.findOne(userId, task.getProjectId()).getName());
            }
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
