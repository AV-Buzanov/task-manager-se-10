package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Task;

import java.util.Objects;

public class TaskViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View task information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE TASK TO VIEW]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        terminalService.printLine(taskService.getList(userId));
        @Nullable String idBuf = taskService.getIdByCount(userId, Integer.parseInt(terminalService.readLine()));
        @Nullable final Task task = taskService.findOne(userId, idBuf);
        terminalService.printWBSLine(task);
        terminalService.printG("[PROJECT]");
        terminalService.printLine(projectService.findOne(userId, task.getProjectId()).getName());
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
