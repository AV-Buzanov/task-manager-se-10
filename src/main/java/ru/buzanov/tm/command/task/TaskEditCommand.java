package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Task;

import java.util.Objects;

public class TaskEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        terminalService.printLineG("[CHOOSE TASK TO EDIT]");
        terminalService.printLine(taskService.getList(userId));
        @Nullable String stringBuf = taskService.getIdByCount(userId, Integer.parseInt(terminalService.readLine()));
        @NotNull final Task task = Objects.requireNonNull(taskService.findOne(userId, stringBuf));
        terminalService.printLineG("[NAME]");
        terminalService.printLine(task.getName());
        terminalService.printLineG("[ENTER NEW NAME]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty())
            taskService.setName(stringBuf, task);
        terminalService.readWBS(task);
        terminalService.printLineG("[PROJECT]");
        terminalService.printLine(projectService.findOne(userId, task.getProjectId()).getName());
        terminalService.printLineG("[CHOOSE NEW PROJECT, WRITE 0 TO REMOVE]");
        terminalService.printLine(projectService.getList(userId));
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()) {
            if ("0".equals(stringBuf))
                stringBuf = null;
            else stringBuf = projectService.getIdByCount(userId, Integer.parseInt(stringBuf));
            task.setProjectId(stringBuf);
        }
        taskService.merge(userId, task.getId(), task);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
