package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;

import java.util.Objects;

public class TaskClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        taskService.removeAll(userId);
        terminalService.printLineG("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
