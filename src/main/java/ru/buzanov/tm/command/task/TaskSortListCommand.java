package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Task;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TaskSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks sorted";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[SORTED TASK LIST]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        @NotNull List<Task> list = (List<Task>) taskService.findAll(userId);
        terminalService.printLineG("[SORT BY]");
        terminalService.printLine("1 : By creation");
        terminalService.printLine("2 : By name");
        terminalService.printLine("3 : By start date");
        terminalService.printLine("4 : By end date");
        terminalService.printLine("5 : By status");
        @NotNull final String comp = terminalService.readLine();
        terminalService.printLineG("[DIRECTION]");
        terminalService.printLine("1 : Rising");
        terminalService.printLine("2 : Falling");
        boolean dir = true;
        if ("2".equals(terminalService.readLine()))
            dir = false;

        switch (comp) {
            case ("1"):
                if (dir)
                    break;
                else
                    Collections.reverse(Objects.requireNonNull(list));
                break;
            case ("2"):
                list.sort(taskService.getNameComparator(dir));
                break;
            case ("3"):
                list.sort(taskService.getStartDateComparator(dir));
                break;
            case ("4"):
                list.sort(taskService.getEndDateComparator(dir));
                break;
            case ("5"):
                list.sort(taskService.getStatusComparator(dir));
                break;
            default:
                break;
        }

        for (@NotNull final Task task : list) {
            terminalService.printWBS(task);
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
