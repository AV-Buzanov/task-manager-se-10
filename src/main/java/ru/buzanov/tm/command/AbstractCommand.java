package ru.buzanov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.util.TerminalService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    protected ITerminalService terminalService;

    protected IUserService userService;

    protected IProjectService projectService;

    protected ITaskService taskService;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
        this.userService = serviceLocator.getUserService();
        this.projectService = serviceLocator.getProjectService();
        this.taskService = serviceLocator.getTaskService();
    }

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean isSecure() throws Exception;

    public boolean isRoleAllow(RoleType role) {
        return true;
    }

}
