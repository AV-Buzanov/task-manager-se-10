package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;

import java.util.Objects;

public class ProjectViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View project information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE PROJECT TO VIEW]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        terminalService.printLine(projectService.getList(userId));
        String idBuf = projectService.getIdByCount(userId, Integer.parseInt(terminalService.readLine()));
        @NotNull final Project project = projectService.findOne(userId, idBuf);
        terminalService.printWBSLine(project);
        terminalService.printLineG( "[TASKS(", String.valueOf(taskService.findByProjectId(userId, idBuf).size()), ")]");
        if (taskService.findByProjectId(userId, idBuf).isEmpty()) {
            terminalService.printLine(FormatConst.EMPTY_FIELD);
            return;
        }
        for (@NotNull final Task task : taskService.findByProjectId(userId, idBuf)) {
            terminalService.printLine(task.getName(), " : ", task.getStatus().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
