package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Project;

import java.util.Objects;

public class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String userId = userService.getCurrentUser().getId();
        for (@NotNull final Project project : projectService.findAll(userId))
            taskService.removeByProjectId(userId, project.getId());

        projectService.removeAll(userId);
        terminalService.printLineG("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
