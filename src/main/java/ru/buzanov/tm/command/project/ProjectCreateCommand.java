package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Project;

import java.util.Objects;

public class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Project project = new Project();
        @NotNull final String userId = userService.getCurrentUser().getId();
        project.setUserId(userId);
        terminalService.printLineG( "[PROJECT CREATE]");
        terminalService.printG( "[ENTER NAME] ");
        projectService.setName(terminalService.readLine(), project);
        terminalService.printLineG("[FILL DATA(Y/N)?]");
        if (terminalService.readLine().equals("Y"))
            terminalService.readWBS(project);
        projectService.load(project);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
