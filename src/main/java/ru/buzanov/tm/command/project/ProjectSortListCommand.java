package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class ProjectSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects sorted";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[SORTED PROJECT LIST]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        @NotNull List<Project> list = (List<Project>) projectService.findAll(userId);

        terminalService.printLineG("[SORT BY]");
        terminalService.printLine("1 : By creation");
        terminalService.printLine("2 : By name");
        terminalService.printLine("3 : By start date");
        terminalService.printLine("4 : By end date");
        terminalService.printLine("5 : By tasks");
        terminalService.printLine("6 : By status");
        @NotNull final String comp = terminalService.readLine();
        terminalService.printLineG("[DIRECTION]");
        terminalService.printLine("1 : Rising");
        terminalService.printLine("2 : Falling");
        boolean dir = true;
        if ("2".equals(terminalService.readLine()))
            dir = false;


        switch (comp) {
            case ("1"):
                if (dir)
                    break;
                else
                    Collections.reverse(Objects.requireNonNull(list));
                break;
            case ("2"):
                list.sort(projectService.getNameComparator(dir));
                break;
            case ("3"):
                list.sort(projectService.getStartDateComparator(dir));
                break;
            case ("4"):
                list.sort(projectService.getEndDateComparator(dir));
                break;
            case ("5"):
                if (dir)
                    list.sort(new Comparator<Project>() {
                        @Override
                        public int compare(Project o1, Project o2) {
                            int o1size = Objects.requireNonNull(taskService.findByProjectId(o1.getId())).size();
                            int o2size = Objects.requireNonNull(taskService.findByProjectId(o2.getId())).size();
                            return Integer.compare(o1size, o2size);
                        }
                    });
                else
                    list.sort(new Comparator<Project>() {
                        @Override
                        public int compare(Project o1, Project o2) {
                            int o1size = Objects.requireNonNull(taskService.findByProjectId(o1.getId())).size();
                            int o2size = Objects.requireNonNull(taskService.findByProjectId(o2.getId())).size();
                            return Integer.compare(o1size, o2size) * (-1);
                        }
                    });
                break;
            case ("6"):
                list.sort(projectService.getStatusComparator(dir));
                break;
            default:
                break;
        }

        for (@NotNull final Project project : list) {
            terminalService.printWBS(project);
            terminalService.printLine();
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
