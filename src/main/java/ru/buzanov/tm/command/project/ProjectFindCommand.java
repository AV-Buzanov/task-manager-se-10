package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectFindCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-find";
    }

    @NotNull
    @Override
    public String description() {
        return "Find projects by name or description.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        @NotNull List<Project> list = new ArrayList<>();
        terminalService.printLineG( "[FIND BY]");
        terminalService.printLine("1 : Name");
        terminalService.printLine("2 : Description");
        @NotNull final String s = terminalService.readLine();
        switch (s) {
            case ("1"):
                terminalService.printLineG("[ENTER NAME]");
                list = (List<Project>) projectService.findByName(userId, terminalService.readLine());
                break;
            case ("2"):
                terminalService.printLineG("[ENTER DESCRIPTION]");
                list = (List<Project>) projectService.findByDescription(userId, terminalService.readLine());
                break;
            default:
                break;
        }

        if (list.isEmpty())
            terminalService.printLine(FormatConst.EMPTY_FIELD);
        else {
            for (@NotNull final Project project : list) {
                terminalService.printWBS(project);
                terminalService.printLine();
            }
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
