package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;

import java.util.Objects;

public class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected project with connected tasks.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG( "[CHOOSE PROJECT TO REMOVE]");
        @NotNull final String userId = Objects.requireNonNull(userService.getCurrentUser()).getId();
        terminalService.printLine(projectService.getList(userId));
        String idBuf = projectService.getIdByCount(userId, Integer.parseInt(terminalService.readLine()));
        projectService.remove(userId, Objects.requireNonNull(idBuf));
        taskService.removeByProjectId(userId, idBuf);
        terminalService.printLineG( "[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
