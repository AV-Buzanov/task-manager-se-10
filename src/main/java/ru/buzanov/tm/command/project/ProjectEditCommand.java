package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Project;

import java.util.Objects;

public class ProjectEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG( "[CHOOSE PROJECT TO EDIT]");
        @NotNull final String userId = userService.getCurrentUser().getId();
        terminalService.printLine(projectService.getList(userId));
        String stringBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(terminalService.readLine()));
        if (stringBuf==null)throw new Exception("Wrong index.");
        @Nullable final Project project = serviceLocator.getProjectService().findOne(userId, stringBuf);
        terminalService.printLineG( "[ENTER NEW NAME]");
        stringBuf = terminalService.readLine();
        projectService.setName(stringBuf, project);
        terminalService.readWBS(project);
        projectService.merge(userId, project.getId(), project);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
